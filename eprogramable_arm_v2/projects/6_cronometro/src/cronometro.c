/* Cátedra: Electrónica Programable
 * FIUNER - 2018
 *
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Initials     Name
 * ---------------------------
 * GN		Gainza Nicolas
 * MVLAN	Molina Van Leeuwen Ana Pabla
 * IR		Riveros Ignacio

 */


/*==================[inclusions]=============================================*/
#include "cronometro.h"       /* <= own header */

#include "board.h"
//#include "chip.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint32_t actual_time = 0;
/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
/*rutina systick, interrupcion*/
void alarm(void)
{
	StopCronometer();
	Led_On(RGB_B_LED);
	Led_On(RED_LED);
	Led_Off(GREEN_LED);
}

void time(void)
{
	actual_time = (minutes()*100)+seconds();
	Write_ITS_E0803(actual_time);
}

int main(void)
{
	CRONO crono_time_setting = {5,18,1};
	uint32_t tecla;
	Board_Init();
	Init_ITS_E0803();
	Init_Switches();
	Init_Leds();
	SetCronometerTime(&crono_time_setting);
	InitCronometer(&time,&alarm);

	Led_On(GREEN_LED);

	while(1)
	{
		tecla = Read_Switches();
		switch(tecla)
		{
			case TEC1:
				Write_ITS_E0803(centesimals());
			break;

			case TEC2:		/*Resume Cronometer*/
				ResumeCronometer();
				Led_On(GREEN_LED);
				Led_Off(RED_LED);
				Led_Off(RGB_B_LED);
			break;

			case TEC3:
				ResetCronometer();
				Led_Off(RGB_B_LED);
			break;

			case TEC4:
				StopCronometer();
				Led_On(RED_LED);
				Led_Off(GREEN_LED);
			break;
		}
	}
return 0;
};

/*==================[end of file]============================================*/

