/* Copyright 2016, XXXXXXXXX  
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Blinking Bare Metal driver led
 **
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */

/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup Baremetal Bare Metal LED Driver
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

/*==================[inclusions]=============================================*/

#include "chip.h"
#include "stopwatch.h"
#include "cronometer.h"

//#include "board.h"
#include "chip.h"


/*==================[macros and definitions]=================================*/




/*==================[internal data declaration]==============================*/
uint32_t mins= 0;
uint32_t secs= 0;
uint32_t cents=0;

CRONO cronometer_time = {0,0,0};

void (*pIsrCrono)();
void (*pAlarm)();
/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/
void InitCronometer(void *pfunc, void *pAl)
{
	pIsrCrono=pfunc;
	pAlarm = pAl;
	SystemCoreClockUpdate ();
	SysTick_Config(SystemCoreClock / 100);
}

void SetCronometerTime(CRONO *crono_time)
{
	cronometer_time.cents = crono_time->cents;
	cronometer_time.secs = crono_time->secs;
	cronometer_time.mins = crono_time->mins;
}

void SysTick_Handler(void)
{
	cents ++;
	if(cents == 100)
	{
		secs ++;
		cents =0;

	}
	if(secs == 60)
	{
		secs =0;
		mins ++;
	}
	if (mins == 10)
	{
		mins = 0;
	}
	pIsrCrono();   /*llama a la función de la aplicación apuntada */

	/*check if not 0*/
	if(cronometer_time.cents && cronometer_time.secs && cronometer_time.mins)
	{
		/*Check the Final Time*/
		if(cronometer_time.cents == cents && cronometer_time.secs == secs && cronometer_time.mins == mins)
		{
			pAlarm();
		}
	}
}

uint32_t centesimals(void)
{
	return cents;
}

uint32_t seconds(void)
{
	return secs;
}

uint32_t minutes(void)
{
	return mins;
}

void StopCronometer(void)
{
	SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_ENABLE_Msk;
}

void ResumeCronometer(void)
{
	SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk  | SysTick_CTRL_ENABLE_Msk;

}

void ResetCronometer(void)
{
	cents = 0;
	secs = 0;
	mins = 0;
}



/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/
/** \brief Main function
 *
 * This is the main entry point of the software.
 *
 * \returns 0
 *
 * \remarks This function never returns. Return value is only to avoid compiler
 *          warnings or errors.
 */




/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

