/* Cátedra: Electrónica Programable
 * FIUNER - 2018
 *
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */



/*
 * Initials     Name
 * ---------------------------
 * IR		Riveros Ignacio
 * NG		Nicolas Gainza
 * APMVL	Ana Pabla Molina Van Leeuwen
 * JMR		JM Reta. Revisión
 */


/** \brief Bare Metal driver for LCD Poncho in the EDU-CIAA board.
 **
 **/

/*==================[inclusions]=============================================*/
#include "lcd_ITSE0803.h"

#include "chip.h"


/*==================[macros and definitions]=================================*/
/** Mapping LCD pins

 * P4_4  in GPIO 2[4] -> LCD01 and pull up.
 * P4_5  in GPIO 2[5] -> LCD02 and pull up.
 * P4_6  in GPIO 2[6] -> LCD03 and pull up.
 * P4_10 in GPIO 5[14] -> LCD04 and pull up.
 *
 * Pins of control
 * P6_4  in GPIO 3[3] -> GPIO01
 * P6_7  in GPIO 5[15] -> GPIO03
 * P6_9  in GPIO 3[5] -> GPIO05
 * */

#define LCD_1_MUX_GROUP	4
#define LCD_2_MUX_GROUP	4
#define LCD_3_MUX_GROUP	4
#define LCD_4_MUX_GROUP	4

#define LCD_SEL1_MUX_GROUP	6
#define LCD_SEL2_MUX_GROUP	6
#define LCD_SEL3_MUX_GROUP	6

#define LCD_1_MUX_PIN	4
#define LCD_2_MUX_PIN	5
#define LCD_3_MUX_PIN	6
#define LCD_4_MUX_PIN	10

#define LCD_SEL1_MUX_PIN	4
#define LCD_SEL2_MUX_PIN	7
#define LCD_SEL3_MUX_PIN	9

#define LCD1_GPIO_PORT	2
#define LCD2_GPIO_PORT	2
#define LCD3_GPIO_PORT	2
#define LCD4_GPIO_PORT	5

#define LCD_SEL1_GPIO_PORT	3
#define LCD_SEL2_GPIO_PORT	5
#define LCD_SEL3_GPIO_PORT	3

#define LCD1_GPIO_PIN 4
#define LCD2_GPIO_PIN 5
#define LCD3_GPIO_PIN 6
#define LCD4_GPIO_PIN 14

#define GPIO1_PIN 3
#define GPIO3_PIN 15
#define GPIO5_PIN 5

#define OUTPUT 1

uint16_t valor_actual=0;// variable that saves the value to be shown in the display LCD

 /** \brief Initialization function of EDU-CIAA LCD Modules
  **
  ** \param[in] No parameter
  **
  ** \param[out] No parameter
  **/
void Init_ITS_E0803(void)
{
	/** Configuration of the GPIO port*/
	Chip_GPIO_Init(LPC_GPIO_PORT);

	/** Mapping of LCD01
	 * P4_4  en GPIO 2[4] -> LCD01 and pull up. */
		Chip_SCU_PinMux(LCD_1_MUX_GROUP,LCD_1_MUX_PIN,MD_PUP,FUNC0);
	/** Set LCD01 port as output*/
		Chip_GPIO_SetDir(LPC_GPIO_PORT, LCD1_GPIO_PORT ,1<<LCD1_GPIO_PIN ,OUTPUT);

	/** Mapping of LCD02
	 P4_5  in GPIO 2[5] -> LCD02 and pull up. */
		Chip_SCU_PinMux(LCD_2_MUX_GROUP,LCD_2_MUX_PIN,MD_PUP,FUNC0);
	/** Set LCD02 port as output*/
		Chip_GPIO_SetDir(LPC_GPIO_PORT, LCD2_GPIO_PORT ,1<<LCD2_GPIO_PIN ,OUTPUT);


	/** Mapping of LCD03
	 P4_6  in GPIO 2[6] -> LCD03 and pull up. */
		Chip_SCU_PinMux(LCD_3_MUX_GROUP,LCD_3_MUX_PIN,MD_PUP,FUNC0);
	/** Set LCD03 port as output*/
		Chip_GPIO_SetDir(LPC_GPIO_PORT, LCD3_GPIO_PORT ,1<<LCD3_GPIO_PIN ,OUTPUT);

	/** Mapping of LCD04
	 P4_10 in GPIO 5[14] -> LCD04 and pull up. */
		Chip_SCU_PinMux(LCD_4_MUX_GROUP,LCD_4_MUX_PIN,MD_PUP,FUNC4);
	/** Set LCD04 port as output*/
		Chip_GPIO_SetDir(LPC_GPIO_PORT, LCD4_GPIO_PORT ,1<<LCD4_GPIO_PIN ,OUTPUT);



	/*Mapping Pines of Control*/

	/* P6_4  in GPIO 3[3] -> GPIO01. */
		Chip_SCU_PinMux(LCD_SEL1_MUX_GROUP,LCD_SEL1_MUX_PIN,MD_PUP,FUNC0);
	/* Set GPIO01 port as output*/
		Chip_GPIO_SetDir(LPC_GPIO_PORT, LCD_SEL1_GPIO_PORT ,1<<GPIO1_PIN ,OUTPUT);

	/** P6_7  in GPIO 5[15] -> GPIO03. */
		Chip_SCU_PinMux(LCD_SEL2_MUX_GROUP,LCD_SEL2_MUX_PIN,MD_PUP,FUNC4);
	/** Set GPIO03 port as output*/
		Chip_GPIO_SetDir(LPC_GPIO_PORT, LCD_SEL2_GPIO_PORT ,1<<GPIO3_PIN ,OUTPUT);

	/** P6_9  in GPIO 3[5] -> GPIO05. */
		Chip_SCU_PinMux(LCD_SEL3_MUX_GROUP,LCD_SEL3_MUX_PIN,MD_PUP,FUNC0);
	/** Set GPIO05 port as output*/
		Chip_GPIO_SetDir(LPC_GPIO_PORT, LCD_SEL3_GPIO_PORT ,1<<GPIO5_PIN ,OUTPUT);

	/* Inicalizing LCD in Low */
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
	/* Inicalizing Control LCD in Low */
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_SEL1_GPIO_PORT,GPIO1_PIN);
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_SEL2_GPIO_PORT,GPIO3_PIN);
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_SEL3_GPIO_PORT,GPIO5_PIN);

		 valor_actual=0;

};


 /*Auxiliar function to load a digit to the LCD Display*/

 void CargarCifra(uint16_t cifra){
 	switch (cifra)
 				{
 		/* load value 0 */
 					case 0:{
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 				}
 		/* Load value 1 */
 					case 1:
 					{
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 					}
 		/* Load vale 2 */
 					case 2:
 					{
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 					}
 		/* Load value 3 */
 					case 3:
 					{
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 					}
 		/* Load value 4 */
 					case 4:
 					{
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 					}
 		/* Load value 5 */
 					case 5:
 					{
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 					}
 		/* Load value 6 */
 					case 6:
 					{
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 					}
 		/* Load value 7*/
 					case 7:
 					{
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 					}
 		/* Load value 8*/
 					case 8:
 					{
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 					}
 		/* Load value 9 */
 					case 9:
 					{
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD1_GPIO_PORT,LCD1_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD2_GPIO_PORT,LCD2_GPIO_PIN);
 						Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD3_GPIO_PORT,LCD3_GPIO_PIN);
 						Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);
 						break;
 					}

 				};

 };

 /** \brief Function to turn on a LCD Module for EDU-CIAA
  **
  ** \param[in] value to show 0..999
  **
  ** \return -1 if an error occurs (out of scale), in other case returns 0
  **/
 int8_t Write_ITS_E0803(uint16_t valor)
 {
	 valor_actual=valor;
	 int8_t resultado=0; //Variable auxiliar para el return de la funcion.

	 if(valor<1000)
	 	 {
		 uint16_t centena=valor/100;	/*Segunda Cifra del Display*/
		 uint16_t decena=(valor-(centena*100))/10;	/*Tercera Cifra del Display*/
		 uint16_t unidad=(valor-(centena*100)-(decena*10));	/*Cuarta Cifra del Display*/

		 /*Para mostrar una cifra, primero se carga el valor y despues se envia un 1 al SEL que desee
		 * mostrar.
		 * SEL1->Centena
		 * SEL2->Decena
		 * SEL3->Unidad
		 * */

		 CargarCifra(centena);
		 /*Generando el pulso para mostrar el numero cargado en la centena*/
		 Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_SEL1_GPIO_PORT,GPIO1_PIN);
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_SEL1_GPIO_PORT,GPIO1_PIN);

		 CargarCifra(decena);
		 /*Generando el pulso para mostrar el numero cargado en la decena*/
		 Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_SEL2_GPIO_PORT,GPIO3_PIN);
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_SEL2_GPIO_PORT,GPIO3_PIN);

		 CargarCifra(unidad);
		 /*Generando el pulso para mostrar el numero cargado en la unidad*/
		 Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_SEL3_GPIO_PORT,GPIO5_PIN);
		 Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_SEL3_GPIO_PORT,GPIO5_PIN);


		 resultado=0;/*Si valor es menor que 999 la funcion devuelve 0*/

	 	 }
	 else {resultado=-1;} /*Si valor es mayor que 999 la funcion devuelve -1*/

 return (resultado);
 };


 /** \brief Function to read a number in LCD Module
  **
  ** \param[in] No parameter
  **
  ** \return number in LCD Module ..999
  **/
uint16_t Read_ITS_E0803(void){
	return (valor_actual);
};

/** \brief DesInitialization function of EDU-CIAA LCD Module
 **
 ** \param[in] No parameter
 **
 ** \param[out] No parameter
 **/
void DesInicializarDisplayITS_E0803(void){
	Chip_GPIO_DeInit(LPC_GPIO_PORT);

}

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
